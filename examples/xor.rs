use rsnn::nn::{self, Network};

fn main() {
    let mut net = Network::new(&[2, 2, 1], nn::Atan);
    for _ in 0..100000 {
        let x = rand::random::<bool>() as i32;
        let y = rand::random::<bool>() as i32;
        let input = rsnn::vec(vec![x as f32, y as f32]);
        let result = net.feedforward(&input);
        let desired = rsnn::vec(vec![(x ^ y) as f32]);
        let cost = nn::cost(&result, &desired);
        net.update(input, desired, cost);
    }
    for x in 0..=1 {
        for y in 0..=1 {
            let input = rsnn::vec(vec![x as f32, y as f32]);
            let result = net.feedforward(&input);
            println!("{} ^ {} = {}", x, y, result[(0, 0)]);
        }
    }
}
