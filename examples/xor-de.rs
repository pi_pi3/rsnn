use std::fs::File;
use std::io::Read;

use serde::Deserialize;
use ron::de::Deserializer;

use rsnn::nn::{self, Network};

fn main() {
    let mut file = File::open("xor.ron").unwrap();
    let mut input = String::new();
    file.read_to_string(&mut input).unwrap();
    let mut de = Deserializer::from_str(&input).unwrap();
    let mut net = Network::<nn::Atan>::deserialize(&mut de).unwrap();

    for x in 0..=1 {
        for y in 0..=1 {
            let input = rsnn::vec(vec![x as f32, y as f32]);
            let result = net.feedforward(&input);
            println!("{} ^ {} = {}", x, y, result[(0, 0)]);
        }
    }
}
