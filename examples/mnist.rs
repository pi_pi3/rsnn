use mnist::MnistBuilder;

use rsnn::nn::{self, Network};

fn main() {
    let trn_size = 60_000;
    let test_size = 10_000;
    let rows = 28;
    let cols = 28;
    let mnist = MnistBuilder::new()
        .label_format_digit()
        .training_set_length(trn_size as u32)
        .validation_set_length(0)
        .test_set_length(test_size as u32)
        .finalize();

    let mut net = Network::new(&[rows * cols, 32, 16, 10], nn::Sigmoid);
    for i in 0..trn_size {
        let vec = mnist.trn_img[i * rows * cols..(i + 1) * rows * cols]
            .iter()
            .map(|b| *b as f32 / 255.)
            .collect();
        let input = rsnn::vec(vec);
        let result = net.feedforward(&input);
        let mut vec = vec![0.; 10];
        vec[mnist.trn_lbl[i] as usize] = 1.;
        let desired = rsnn::vec(vec);
        let cost = nn::cost(&result, &desired);
        net.update(input, desired, cost);
        if i % 10 == 0 {
            eprintln!("{}/{}", i + 1, trn_size);
        }
    }
    for i in 0..test_size {
        let vec = mnist.tst_img[i * rows * cols..(i + 1) * rows * cols]
            .iter()
            .map(|b| *b as f32 / 255.)
            .collect();
        let input = rsnn::vec(vec);
        let result = net.feedforward(&input);
        let mut n = 0;
        let mut f = 0.;
        for m in 0..10 {
            if result[(m, 0)] > f {
                f = result[(m, 0)];
                n = m;
            }
        }
        let correct = if mnist.tst_lbl[i] == n as u8 {
            "correct"
        } else {
            "incorrect"
        };
        if i % 10 == 0 {
            eprintln!("{}/{}", i + 1, test_size);
        }
        println!("=> {} ({}) ({})", n, f, correct);
    }
}
