use std::fmt::{self, Debug, Display};
use std::ops::{Index, IndexMut, Neg, Add, Sub, Mul};

pub type Vector = Matrix;

#[derive(Clone, PartialEq, Serialize, Deserialize)]
pub struct Matrix {
    storage: Vec<f32>,
    rows: usize,
    cols: usize,
}

impl Matrix {
    pub fn zero(rows: usize, cols: usize) -> Self {
        Self::matrix(rows, cols, vec![0.; rows * cols])
    }

    pub fn one(rows_cols: usize) -> Self {
        let mut output = Self::zero(rows_cols, rows_cols);
        for k in 0..rows_cols {
            output[(k, k)] = 1.;
        }
        output
    }

    pub fn new(rows: usize, cols: usize, storage: Vec<f32>) -> Self {
        Self::matrix(rows, cols, storage)
    }

    pub fn matrix(rows: usize, cols: usize, storage: Vec<f32>) -> Self {
        assert_eq!(storage.len(), rows * cols, "invalid matrix dimensions");
        Matrix { storage, rows, cols }
    }

    pub fn vector(storage: Vec<f32>) -> Self {
        Matrix::matrix(storage.len(), 1, storage)
    }

    pub fn into_inner(self) -> Vec<f32> {
        self.storage
    }

    pub fn inner(&self) -> &[f32] {
        &self.storage
    }

    pub fn rows(&self) -> usize {
        self.rows
    }

    pub fn cols(&self) -> usize {
        self.cols
    }

    pub fn iter<'a>(&'a self) -> impl Iterator<Item = f32> + 'a {
        self.storage.iter().cloned()
    }

    pub fn component_mul(&self, other: &Matrix) -> Matrix {
        assert_eq!(self.rows, other.rows, "incompatible matrices");
        assert_eq!(self.cols, other.cols, "incompatible matrices");

        let mut output = Matrix::matrix(self.rows, self.cols, vec![0.; self.rows * self.cols]);
        for j in 0..self.cols {
            for i in 0..self.rows {
                output[(i, j)] = self[(i, j)] * other[(i, j)];
            }
        }
        output
    }

    pub fn transpose(&self) -> Matrix {
        let mut output = Matrix::matrix(self.cols, self.rows, vec![0.; self.rows * self.cols]);
        for j in 0..output.cols {
            for i in 0..output.rows {
                output[(i, j)] = self[(j, i)];
            }
        }
        output
    }

    pub fn map<F: Fn(f32) -> f32>(&self, func: F) -> Matrix {
        Matrix::matrix(self.rows, self.cols, self.storage.iter().cloned().map(func).collect())
    }
}

impl Index<(usize, usize)> for Matrix {
    type Output = f32;

    fn index(&self, (i, j): (usize, usize)) -> &f32 {
        assert!(j < self.cols, "index out of bounds");
        assert!(i < self.rows, "index out of bounds");
        &self.storage[i * self.cols + j]
    }
}

impl IndexMut<(usize, usize)> for Matrix {
    fn index_mut(&mut self, (i, j): (usize, usize)) -> &mut f32 {
        &mut self.storage[i * self.cols + j]
    }
}

impl Neg for Matrix {
    type Output = Self;

    fn neg(self) -> Self {
        -(&self)
    }
}

impl<'a> Neg for &'a Matrix {
    type Output = Matrix;

    fn neg(self) -> Matrix {
        let mut output = Matrix::zero(self.rows, self.cols);
        for j in 0..self.cols {
            for i in 0..self.rows {
                output[(i, j)] = -self[(i, j)];
            }
        }
        output
    }
}

impl Add for Matrix {
    type Output = Matrix;

    fn add(self, other: Matrix) -> Matrix {
        (&self) + (&other)
    }
}

impl<'a> Add<Matrix> for &'a Matrix {
    type Output = Matrix;

    fn add(self, other: Matrix) -> Matrix {
        self + (&other)
    }
}

impl<'a> Add<&'a Matrix> for Matrix {
    type Output = Matrix;

    fn add(self, other: &'a Matrix) -> Matrix {
        (&self) + other
    }
}

impl<'a> Add for &'a Matrix {
    type Output = Matrix;

    fn add(self, other: &'a Matrix) -> Matrix {
        assert_eq!(self.rows, other.rows, "incompatible matrices");
        assert_eq!(self.cols, other.cols, "incompatible matrices");

        let mut output = Matrix::zero(self.rows, self.cols);
        for j in 0..self.cols {
            for i in 0..self.rows {
                output[(i, j)] = self[(i, j)] + other[(i, j)];
            }
        }
        output
    }
}

impl Sub for Matrix {
    type Output = Matrix;

    fn sub(self, other: Matrix) -> Matrix {
        (&self) - (&other)
    }
}

impl<'a> Sub<Matrix> for &'a Matrix {
    type Output = Matrix;

    fn sub(self, other: Matrix) -> Matrix {
        self - (&other)
    }
}

impl<'a> Sub<&'a Matrix> for Matrix {
    type Output = Matrix;

    fn sub(self, other: &'a Matrix) -> Matrix {
        (&self) - other
    }
}

impl<'a> Sub for &'a Matrix {
    type Output = Matrix;

    fn sub(self, other: &'a Matrix) -> Matrix {
        assert_eq!(self.rows, other.rows, "incompatible matrices");
        assert_eq!(self.cols, other.cols, "incompatible matrices");

        let mut output = Matrix::zero(self.rows, self.cols);
        for j in 0..self.cols {
            for i in 0..self.rows {
                output[(i, j)] = self[(i, j)] - other[(i, j)];
            }
        }
        output
    }
}

impl Mul for Matrix {
    type Output = Matrix;

    fn mul(self, other: Matrix) -> Matrix {
        (&self) * (&other)
    }
}

impl<'a> Mul<Matrix> for &'a Matrix {
    type Output = Matrix;

    fn mul(self, other: Matrix) -> Matrix {
        self * (&other)
    }
}

impl<'a> Mul<&'a Matrix> for Matrix {
    type Output = Matrix;

    fn mul(self, other: &'a Matrix) -> Matrix {
        (&self) * other
    }
}

impl<'a> Mul for &'a Matrix {
    type Output = Matrix;

    fn mul(self, other: &'a Matrix) -> Matrix {
        assert_eq!(self.cols, other.rows, "incompatible matrices");

        let mut output = Matrix::zero(self.rows, other.cols);
        for j in 0..other.cols {
            for i in 0..self.rows {
                for k in 0..self.cols {
                    output[(i, j)] += self[(i, k)] * other[(k, j)];
                }
            }
        }
        output
    }
}

impl Mul<f32> for Matrix {
    type Output = Matrix;

    fn mul(self, other: f32) -> Matrix {
        (&self) * other
    }
}

impl<'a> Mul<&'a f32> for Matrix {
    type Output = Matrix;

    fn mul(self, other: &f32) -> Matrix {
        (&self) * (*other)
    }
}

impl<'a> Mul<&'a f32> for &'a Matrix {
    type Output = Matrix;

    fn mul(self, other: &f32) -> Matrix {
        self * (*other)
    }
}

impl<'a> Mul<f32> for &'a Matrix {
    type Output = Matrix;

    fn mul(self, other: f32) -> Matrix {
        let mut output = Matrix::zero(self.rows, self.cols);
        for j in 0..self.cols {
            for i in 0..self.rows {
                output[(i, j)] += self[(i, j)] * other
            }
        }
        output
    }
}

impl Mul<Matrix> for f32 {
    type Output = Matrix;

    fn mul(self, other: Matrix) -> Matrix {
        self * (&other)
    }
}

impl<'a> Mul<Matrix> for &'a f32 {
    type Output = Matrix;

    fn mul(self, other: Matrix) -> Matrix {
        (*self) * (&other)
    }
}

impl<'a> Mul<&'a Matrix> for &'a f32 {
    type Output = Matrix;

    fn mul(self, other: &'a Matrix) -> Matrix {
        (*self) * other
    }
}

impl<'a> Mul<&'a Matrix> for f32 {
    type Output = Matrix;

    fn mul(self, other: &'a Matrix) -> Matrix {
        let mut output = Matrix::zero(other.rows, other.cols);
        for j in 0..other.cols {
            for i in 0..other.rows {
                output[(i, j)] += other[(i, j)] * self
            }
        }
        output
    }
}

impl Debug for Matrix {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[")?;
        for i in 0..self.rows {
            for j in 0..self.cols {
                write!(f, " {}", self[(i, j)])?;
            }
            write!(f, ";")?;
        }
        write!(f, " ]")
    }
}

impl Display for Matrix {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for i in 0..self.rows {
            write!(f, "| ")?;
            for j in 0..self.cols {
                write!(f, "{} ", self[(i, j)])?;
            }
            writeln!(f, "|")?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sanity() {
        let a = &Matrix::new(3, 3, vec![1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]);
        let b = &Matrix::new(3, 3, vec![0.5, 0.1, 0.3, 0.4, 0.9, 0.7, 0.2, 0.8, 0.6]);
        assert_eq!(a * b, *b);
        assert_eq!(b * a, *b);
        assert_eq!(
            a + b,
            Matrix::new(3, 3, vec![1.5, 0.1, 0.3, 0.4, 1.9, 0.7, 0.2, 0.8, 1.6])
        );
    }
}
