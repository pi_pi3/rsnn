use std::f32;
use std::iter;

use rand::Rng;
use rand::FromEntropy;
use rand::rngs::SmallRng;
use rand::distributions::StandardNormal;

use crate::algebra::{Matrix, Vector};
use crate::nn::{self, Activation};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Network<F> {
    weights: Vec<Matrix>,
    biases: Vec<Vector>,
    memory: Vector,
    act: F,
}

impl<F: Activation> Network<F> {
    pub fn new(layers: &[usize], act: F) -> Self {
        let mut weights = vec![];
        let mut biases = vec![];
        let mut rng = SmallRng::from_entropy();
        for (&input, &output) in iter::once(&(layers[0] + *layers.last().unwrap()))
            .chain(layers.iter().skip(1))
            .zip(layers.iter().skip(1))
        {
            let weight_size = input * output;
            let weight = (0..weight_size).map(|_| rng.sample(StandardNormal) as f32).collect();
            weights.push(Matrix::matrix(output, input, weight));
            let bias = (0..output).map(|_| rng.sample(StandardNormal) as f32).collect();
            biases.push(Vector::vector(bias));
        }
        Self::with(weights, biases, Vector::zero(*layers.last().unwrap(), 1), act)
    }

    pub fn with(weights: Vec<Matrix>, biases: Vec<Vector>, memory: Vector, act: F) -> Self {
        assert_eq!(weights.len(), biases.len(), "incompatible weights and biases");
        assert!(weights.len() >= 1, "network must have at least one layer");
        for i in 0..weights.len() - 1 {
            assert_eq!(weights[i].rows(), weights[i + 1].cols(), "incompatible weights");
        }
        for i in 0..weights.len() {
            assert_eq!(weights[i].rows(), biases[i].rows(), "incompatible weights and biases");
        }
        Network {
            weights,
            biases,
            memory,
            act,
        }
    }

    pub fn into_inner(self) -> (Vec<Matrix>, Vec<Vector>, F) {
        (self.weights, self.biases, self.act)
    }

    pub fn inner(&self) -> (&[Matrix], &[Vector], &F) {
        (&self.weights, &self.biases, &self.act)
    }

    pub fn layers(&self) -> usize {
        self.weights.len()
    }

    pub fn feedforward(&mut self, layer: &Vector) -> Vector {
        let layer = Vector::vector(layer.iter().chain(self.memory.iter()).collect());
        let mut output = None;
        for (weight, bias) in self.weights.iter().zip(&self.biases) {
            let layer = output.as_ref().unwrap_or(&layer);
            output = Some((weight * layer + bias).map(|n| self.act.call(n)))
        }
        self.memory = output.as_ref().unwrap().clone();
        output.unwrap()
    }

    pub fn update_mini_batched(&mut self, input: Vec<Vector>, desired: Vec<Vector>, eta: f32) {
        let mut nabla_b = Vec::with_capacity(self.biases.len());
        let mut nabla_w = Vec::with_capacity(self.weights.len());
        for bias in &self.biases {
            nabla_b.push(Vector::zero(bias.rows(), 1));
        }
        for weight in &self.weights {
            nabla_w.push(Matrix::zero(weight.rows(), weight.cols()));
        }

        let len = input.len();
        let eta = eta / len as f32;
        for (input, desired) in input.into_iter().zip(desired) {
            let (delta_nabla_b, delta_nabla_w) = self.backprop(input, desired);

            for (nb, dnb) in nabla_b.iter_mut().zip(delta_nabla_b) {
                *nb = (&*nb) + dnb;
            }

            for (nw, dnw) in nabla_w.iter_mut().zip(delta_nabla_w) {
                *nw = (&*nw) + dnw;
            }

            for (weight, nabla) in self.weights.iter_mut().zip(nabla_w.iter()) {
                *weight = (&*weight) - nabla * eta;
            }

            for (bias, nabla) in self.biases.iter_mut().zip(nabla_b.iter()) {
                *bias = (&*bias) - nabla * eta;
            }
        }
    }

    pub fn update(&mut self, input: Vector, desired: Vector, eta: f32) {
        let (nabla_b, nabla_w) = self.backprop(input, desired);

        for (weight, nabla) in self.weights.iter_mut().zip(nabla_w) {
            *weight = (&*weight) - nabla * eta;
        }

        for (bias, nabla) in self.biases.iter_mut().zip(nabla_b) {
            *bias = (&*bias) - nabla * eta;
        }
    }

    pub fn backprop(&self, input: Vector, desired: Vector) -> (Vec<Vector>, Vec<Matrix>) {
        fn nabla_w_l(activation: &Vector, delta: &Vector) -> Matrix {
            let mut output = Matrix::matrix(
                delta.rows(),
                activation.rows(),
                vec![0.; delta.rows() * activation.rows()],
            );
            for i in 0..delta.rows() {
                for j in 0..activation.rows() {
                    output[(i, j)] = activation[(j, 0)] * delta[(i, 0)];
                }
            }
            output
        }

        let mut nabla_b = Vec::with_capacity(self.biases.len());
        let mut nabla_w = Vec::with_capacity(self.weights.len());
        for bias in &self.biases {
            nabla_b.push(Vector::vector(vec![0.; bias.rows()]));
        }
        for weight in &self.weights {
            nabla_w.push(Matrix::matrix(
                weight.rows(),
                weight.cols(),
                vec![0.; weight.rows() * weight.cols()],
            ));
        }
        let input = Vector::vector(input.iter().chain(self.memory.iter()).collect());
        let mut activations = vec![input];
        let mut activation = activations.last().unwrap();
        let mut zs = vec![];
        for (bias, weight) in self.biases.iter().zip(&self.weights) {
            let z = weight * activation + bias;
            activations.push(z.map(|z| self.act.call(z)));
            activation = activations.last().unwrap();
            zs.push(z);
        }
        let delta = (activation - desired).component_mul(&zs.last().unwrap().map(|z| self.act.derivative(z)));
        *nabla_w.last_mut().unwrap() = nabla_w_l(&activations[activations.len() - 2], &delta);
        *nabla_b.last_mut().unwrap() = delta;
        for l in 2..=self.layers() {
            let z = &zs[self.layers() - l];
            let der = z.map(|z| self.act.derivative(z));
            let delta = (self.weights[self.layers() - l + 1].transpose() * (&nabla_b[self.layers() - l + 1]))
                .component_mul(&der);
            nabla_w[self.layers() - l] = nabla_w_l(&activations[activations.len() - l - 1], &delta);
            nabla_b[self.layers() - l] = delta;
        }
        (nabla_b, nabla_w)
    }
}

impl<F: Activation + Clone> Network<F> {
    pub fn split(&self) -> (nn::Network<F>, nn::Network<F>) {
        let len = (self.layers() + 1) / 2;
        let aw = self.weights[..len].to_vec();
        let ab = self.biases[..len].to_vec();
        let bw = self.weights[self.layers() - len..].to_vec();
        let bb = self.biases[self.layers() - len..].to_vec();
        let a = nn::Network::with(aw, ab, self.act.clone());
        let b = nn::Network::with(bw, bb, self.act.clone());
        (a, b)
    }
}
