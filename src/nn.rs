use std::f32;

use rand::Rng;
use rand::FromEntropy;
use rand::rngs::SmallRng;
use rand::distributions::StandardNormal;

use crate::algebra::{Matrix, Vector};

pub fn cost(result: &Vector, desired: &Vector) -> f32 {
    let diff = result - desired;
    diff.component_mul(&diff).iter().sum::<f32>() * 0.5
}

pub trait Activation {
    fn call(&self, n: f32) -> f32;
    fn derivative(&self, n: f32) -> f32;
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
pub struct Relu;

impl Activation for Relu {
    fn call(&self, n: f32) -> f32 {
        n.max(0.)
    }

    fn derivative(&self, n: f32) -> f32 {
        if n < 0. {
            0.
        } else {
            1.
        }
    }
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
pub struct Binary;

impl Activation for Binary {
    fn call(&self, n: f32) -> f32 {
        if n > 0. {
            1.
        } else {
            0.
        }
    }

    fn derivative(&self, n: f32) -> f32 {
        if n != 0. {
            0.
        } else {
            f32::INFINITY
        }
    }
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
pub struct Tanh;

impl Activation for Tanh {
    fn call(&self, n: f32) -> f32 {
        n.tanh()
    }

    fn derivative(&self, n: f32) -> f32 {
        let tanh = self.call(n);
        1. - tanh * tanh
    }
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
pub struct Atan;

impl Activation for Atan {
    fn call(&self, n: f32) -> f32 {
        n.atan()
    }

    fn derivative(&self, n: f32) -> f32 {
        1. / (n * n + 1.)
    }
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
pub struct Sigmoid;

impl Activation for Sigmoid {
    fn call(&self, n: f32) -> f32 {
        1. / (1. + (-n).exp())
    }

    fn derivative(&self, n: f32) -> f32 {
        let sigmoid = self.call(n);
        sigmoid * (1. - sigmoid)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Network<F> {
    weights: Vec<Matrix>,
    biases: Vec<Vector>,
    act: F,
}

impl<F: Activation> Network<F> {
    pub fn new(layers: &[usize], act: F) -> Self {
        let mut weights = vec![];
        let mut biases = vec![];
        let mut rng = SmallRng::from_entropy();
        for (&input, &output) in layers.iter().zip(layers.iter().skip(1)) {
            let weight_size = input * output;
            let weight = (0..weight_size).map(|_| rng.sample(StandardNormal) as f32).collect();
            weights.push(Matrix::matrix(output, input, weight));
            let bias = (0..output).map(|_| rng.sample(StandardNormal) as f32).collect();
            biases.push(Vector::vector(bias));
        }
        Self::with(weights, biases, act)
    }

    pub fn with(weights: Vec<Matrix>, biases: Vec<Vector>, act: F) -> Self {
        assert_eq!(weights.len(), biases.len(), "incompatible weights and biases");
        assert!(weights.len() >= 1, "network must have at least one layer");
        for i in 0..weights.len() - 1 {
            assert_eq!(weights[i].rows(), weights[i + 1].cols(), "incompatible weights");
        }
        for i in 0..weights.len() {
            assert_eq!(weights[i].rows(), biases[i].rows(), "incompatible weights and biases");
        }
        Network { weights, biases, act }
    }

    pub fn into_inner(self) -> (Vec<Matrix>, Vec<Vector>, F) {
        (self.weights, self.biases, self.act)
    }

    pub fn inner(&self) -> (&[Matrix], &[Vector], &F) {
        (&self.weights, &self.biases, &self.act)
    }

    pub fn layers(&self) -> usize {
        self.weights.len()
    }

    pub fn feedforward(&mut self, layer: &Vector) -> Vector {
        let mut output = None;
        for (weight, bias) in self.weights.iter().zip(&self.biases) {
            let layer = output.as_ref().unwrap_or(layer);
            output = Some((weight * layer + bias).map(|n| self.act.call(n)))
        }
        output.unwrap()
    }

    pub fn update_mini_batched(&mut self, input: Vec<Vector>, desired: Vec<Vector>, eta: f32) {
        let mut nabla_b = Vec::with_capacity(self.biases.len());
        let mut nabla_w = Vec::with_capacity(self.weights.len());
        for bias in &self.biases {
            nabla_b.push(Vector::zero(bias.rows(), 1));
        }
        for weight in &self.weights {
            nabla_w.push(Matrix::zero(weight.rows(), weight.cols()));
        }

        let len = input.len();
        let eta = eta / len as f32;
        for (input, desired) in input.into_iter().zip(desired) {
            let (delta_nabla_b, delta_nabla_w) = self.backprop(input, desired);

            for (nb, dnb) in nabla_b.iter_mut().zip(delta_nabla_b) {
                *nb = (&*nb) + dnb;
            }

            for (nw, dnw) in nabla_w.iter_mut().zip(delta_nabla_w) {
                *nw = (&*nw) + dnw;
            }

            for (weight, nabla) in self.weights.iter_mut().zip(nabla_w.iter()) {
                *weight = (&*weight) - nabla * eta;
            }

            for (bias, nabla) in self.biases.iter_mut().zip(nabla_b.iter()) {
                *bias = (&*bias) - nabla * eta;
            }
        }
    }

    pub fn update(&mut self, input: Vector, desired: Vector, eta: f32) {
        let (nabla_b, nabla_w) = self.backprop(input, desired);

        for (weight, nabla) in self.weights.iter_mut().zip(nabla_w) {
            *weight = (&*weight) - nabla * eta;
        }

        for (bias, nabla) in self.biases.iter_mut().zip(nabla_b) {
            *bias = (&*bias) - nabla * eta;
        }
    }

    pub fn backprop(&self, input: Vector, desired: Vector) -> (Vec<Vector>, Vec<Matrix>) {
        fn nabla_w_l(activation: &Vector, delta: &Vector) -> Matrix {
            let mut output = Matrix::matrix(
                delta.rows(),
                activation.rows(),
                vec![0.; delta.rows() * activation.rows()],
            );
            for i in 0..delta.rows() {
                for j in 0..activation.rows() {
                    output[(i, j)] = activation[(j, 0)] * delta[(i, 0)];
                }
            }
            output
        }

        let mut nabla_b = Vec::with_capacity(self.biases.len());
        let mut nabla_w = Vec::with_capacity(self.weights.len());
        for bias in &self.biases {
            nabla_b.push(Vector::vector(vec![0.; bias.rows()]));
        }
        for weight in &self.weights {
            nabla_w.push(Matrix::matrix(
                weight.rows(),
                weight.cols(),
                vec![0.; weight.rows() * weight.cols()],
            ));
        }
        let mut activations = vec![input];
        let mut activation = activations.last().unwrap();
        let mut zs = vec![];
        for (bias, weight) in self.biases.iter().zip(&self.weights) {
            let z = weight * activation + bias;
            activations.push(z.map(|z| self.act.call(z)));
            activation = activations.last().unwrap();
            zs.push(z);
        }
        let delta = (activation - desired).component_mul(&zs.last().unwrap().map(|z| self.act.derivative(z)));
        *nabla_w.last_mut().unwrap() = nabla_w_l(&activations[activations.len() - 2], &delta);
        *nabla_b.last_mut().unwrap() = delta;
        for l in 2..=self.layers() {
            let z = &zs[self.layers() - l];
            let der = z.map(|z| self.act.derivative(z));
            let delta = (self.weights[self.layers() - l + 1].transpose() * (&nabla_b[self.layers() - l + 1]))
                .component_mul(&der);
            nabla_w[self.layers() - l] = nabla_w_l(&activations[activations.len() - l - 1], &delta);
            nabla_b[self.layers() - l] = delta;
        }
        (nabla_b, nabla_w)
    }
}

impl<F: Activation + Clone> Network<F> {
    pub fn split(&self) -> (Self, Self) {
        let len = (self.layers() + 1) / 2;
        let aw = self.weights[..len].to_vec();
        let ab = self.biases[..len].to_vec();
        let bw = self.weights[self.layers() - len..].to_vec();
        let bb = self.biases[self.layers() - len..].to_vec();
        let a = Network::with(aw, ab, self.act.clone());
        let b = Network::with(bw, bb, self.act.clone());
        (a, b)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sanity() {
        let weights1 = Matrix::matrix(
            4,
            3,
            vec![
                rand::random::<f32>() * 2. - 1.,
                rand::random::<f32>() * 2. - 1.,
                rand::random::<f32>() * 2. - 1.,
                rand::random::<f32>() * 2. - 1.,
                rand::random::<f32>() * 2. - 1.,
                rand::random::<f32>() * 2. - 1.,
                rand::random::<f32>() * 2. - 1.,
                rand::random::<f32>() * 2. - 1.,
                rand::random::<f32>() * 2. - 1.,
                rand::random::<f32>() * 2. - 1.,
                rand::random::<f32>() * 2. - 1.,
                rand::random::<f32>() * 2. - 1.,
            ],
        );
        let bias1 = Vector::vector(vec![
            rand::random::<f32>() * 2. + 1.,
            rand::random::<f32>() * 2. + 1.,
            rand::random::<f32>() * 2. + 1.,
            rand::random::<f32>() * 2. + 1.,
        ]);
        let weights2 = Matrix::matrix(
            3,
            4,
            vec![
                rand::random::<f32>() * 2. - 1.,
                rand::random::<f32>() * 2. - 1.,
                rand::random::<f32>() * 2. - 1.,
                rand::random::<f32>() * 2. - 1.,
                rand::random::<f32>() * 2. - 1.,
                rand::random::<f32>() * 2. - 1.,
                rand::random::<f32>() * 2. - 1.,
                rand::random::<f32>() * 2. - 1.,
                rand::random::<f32>() * 2. - 1.,
                rand::random::<f32>() * 2. - 1.,
                rand::random::<f32>() * 2. - 1.,
                rand::random::<f32>() * 2. - 1.,
            ],
        );
        let bias2 = Vector::vector(vec![
            rand::random::<f32>() * 2. + 1.,
            rand::random::<f32>() * 2. + 1.,
            rand::random::<f32>() * 2. + 1.,
        ]);
        let mut net = Network::with(vec![weights1, weights2], vec![bias1, bias2], Relu);
        let input = Vector::vector(vec![
            rand::random::<f32>(),
            rand::random::<f32>(),
            rand::random::<f32>(),
        ]);
        let result = net.feedforward(&input);
        let desired = Vector::vector(vec![
            rand::random::<f32>(),
            rand::random::<f32>(),
            rand::random::<f32>(),
        ]);
        let cost = cost(&result, &desired);
        net.update(input, desired, cost);
    }
}
