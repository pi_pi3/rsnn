#[macro_use]
extern crate serde_derive;

pub mod algebra;
pub mod nn;
pub mod rnn;

pub use algebra::{Matrix, Vector};

pub fn mat(rows: usize, cols: usize, storage: Vec<f32>) -> Matrix {
    Matrix::matrix(rows, cols, storage)
}

pub fn vec(storage: Vec<f32>) -> Vector {
    Vector::vector(storage)
}
